import laika.ast.Image
import laika.ast.LengthUnit.px
import laika.ast.Path.Root
import laika.helium.Helium
import laika.helium.config.{ButtonLink, Favicon, Teaser, TextLink}
import laika.markdown.github.GitHubFlavor
import laika.parse.code.SyntaxHighlighting
import laika.rewrite.{Version, Versions}
import laika.rewrite.nav.CoverImage
import laika.sbt.LaikaPlugin.autoImport.laikaIncludeEPUB
import laika.theme.config.{Color, FontDefinition, FontStyle}

import java.time.Instant

val scala2Version = "2.13.6"
val townplanVersion = "3.0.0"

val montserratURL = "https://fonts.googleapis.com/css?family=Montserrat"

val theme = Helium.defaults.all
  .themeColors(
    primary = Color.rgb(37, 26, 6),
    secondary = Color.rgb(8, 136, 140),
    primaryMedium = Color.rgb(77, 77, 77),
    primaryLight = Color.rgb(222, 212, 196),
    text = Color.rgb(37, 26, 6),
    background = Color.rgb(255, 255, 255),
    bgGradient = (Color.rgb(222, 212, 196), Color.rgb(244, 233, 215))
  )
  .site
  .landingPage(
    logo = Some(
      Image.internal(
        Root / "images" / "square.png",
        width = Some(px(200)),
        height = Some(px(200)),
        alt = Some("Innovenso Logo")
      )
    ),
    title = Some("Innovenso Townplanner"),
    subtitle = Some("Enterprise Architecture as Code"),
    license = Some("GPL v3"),
    documentationLinks = Seq(
      TextLink.internal(Root / "about.md", "About the project"),
      TextLink.internal(Root / "usage.md", "Getting Started"),
      TextLink.internal(Root / "concepts.md", "Core Concepts"),
      TextLink.internal(Root / "concepts.md", "Roadmap")
    ),
    projectLinks = Seq(
      TextLink.external(
        "https://bitbucket.org/innovenso/workspace/projects/TOWN",
        "Source on Bitbucket"
      ),
      TextLink.external("https://innovenso.com/", "Innovenso")
    ),
    teasers = Seq(
      Teaser(
        "Rich Model",
        "The model spans all levels of Enterprise Architecture, including business capability maps, pace layering, target operating models, decision records, project impacts, team topologies, infrastructure, applications, technologies, and more. It even has a time machine!"
      ),
      Teaser(
        "Export to Everything",
        "Currently diagram renders to SVG and exports to Confluence, Excel and Archimate are the most useful ones. New exports are easy to add."
      ),
      Teaser(
        "Domain Specific Language",
        "Use your favourite IDE to edit the town plan in an easy to understand DSL, and enjoy code completion and compile-time integrity checks."
      ),
      Teaser(
        "Architecture as Code",
        "The model is just text, so it can be managed with Git, with all the joys of version history, pull requests, branches, ..."
      ),
      Teaser(
        "Gradle Plugin",
        "Focus on the model, not on the boilerplate code around it"
      ),
      Teaser(
        "Opinionated",
        "Loosely based on C4 and Archimate, but a model reflecting the Innovenso metamodel currently. It will grow over time, and it might become more open, as the need arises."
      )
    )
  )
  .site
  .favIcons(
    Favicon.internal(Root / "images" / "favicon.png", sizes = "57x57"),
    Favicon.internal(Root / "images" / "favicon72.png", sizes = "72x72"),
    Favicon.internal(Root / "images" / "favicon72.png", sizes = "114x114")
  )
  .site
  .downloadPage(
    title = "Documentation Downloads",
    description = None,
    downloadPath = Root / "downloads",
    includeEPUB = true,
    includePDF = true
  )
  .epub
  .navigationDepth(4)
  .pdf
  .navigationDepth(4)
  .epub
  .coverImages(CoverImage(Root / "images" / "cover.png"))
  .pdf
  .coverImages(CoverImage(Root / "images" / "cover.png"))
  .all
  .metadata(
    title = Some("Innovenso Townplanner"),
    version = Some(townplanVersion),
    description = Some("Enterprise Architecture as Code"),
    authors = Seq("Jurgen Lust"),
    language = Some("en"),
    date = Some(Instant.now)
  )
  .build

lazy val root = project
  .in(file("."))
  .settings(
    name := "Innovenso Townplanner",
    version := townplanVersion,
    scalaVersion := scala2Version,
    laikaIncludeEPUB := true,
    laikaIncludePDF := true,
    laikaTheme := theme,
    laikaExtensions ++= Seq(GitHubFlavor, SyntaxHighlighting),
    laikaConfig := LaikaConfig.defaults
      .withConfigValue("project.version", townplanVersion)
  )
  .enablePlugins(LaikaPlugin)
