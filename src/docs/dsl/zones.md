# Zones

Zones are defined as *Groovy* classes in the `src/main/groovy/townplan` folder of the project, i.e. as Groovy classes in the `townplan` package. In order
to have the townplan actually pick them up and load their contents into the town plan, they also need to be annotated with `@IncludeInTownplan`. Typically,
the contents of the zone are defined using the `concepts` closure inside the constructor of the class.

    package townplan
    
    import com.innovenso.townplan.dsl.Zone
    import com.innovenso.townplan.gradle.IncludeInTownplan
    
    @IncludeInTownplan
    class WayneEnterprises extends Zone {
    
        WayneEnterprises() {
            concepts {
                //put the contents of the town plan here, using Domain Specific Language
            }
        }
    }

Zones can be injected in other zones, using constructor injection. The Townplanner handles the dependency injection between zones, so all you
need to do is add the zones to be injected to the signature of the zone they should be injected into. You just have to make sure that you
don't put any circular dependencies.

    package townplan
    
    import com.innovenso.townplan.dsl.Zone
    import com.innovenso.townplan.gradle.IncludeInTownplan
    import townplan.delorean.TimeMachine
    
    @IncludeInTownplan
    class WayneEnterprises extends Zone {
    
        WayneEnterprises(TimeMachine timeMachine) {
            concepts {
                //put the contents of the town plan here, using Domain Specific Language
            }
        }
    }

In the example above, we have a zone called *TimeMachine* that is injected into the *WayneEnterprises* zone. Now, 
inside the `concepts` closure, we can use references to the timeMachine, and all the properties it exposes.

Suppose the TimeMachine zone looks like this:

    package townplan.delorean
    
    import com.innovenso.townplan.dsl.Zone
    import com.innovenso.townplan.dsl.timemachine.KeyPointInTimeConfigurer as PointInTime
    import com.innovenso.townplan.gradle.IncludeInTownplan
    
    import java.time.LocalDate
    
    @IncludeInTownplan
    class TimeMachine extends Zone {
    PointInTime beforeVillains
    PointInTime batcaveDelivery
    PointInTime destructionDay
    PointInTime today
    
        TimeMachine() {
            concepts {
                timeMachine {
                    this.beforeVillains = pointInTime {
                        title "Before there were villains"
                        date LocalDate.of(2000, 1, 1)
                    }
                    this.batcaveDelivery = pointInTime {
                        title "When project Batcave was deliver on time and within budget"
                        date LocalDate.of(2010, 1, 1)
                    }
                    this.destructionDay = pointInTime {
                        title "At some point in the future, the Riddler destroys the Batcave"
                        date LocalDate.of(2030, 1, 1)
                    }
                    this.today = today()
                }
            }
        }
    }

Then, we can reference the 4 points in time in the *WayneEnterprises* zone:

    ...
    WayneEnterprises(TimeMachine timeMachine) {
        concepts {
            println timeMachine.beforeVillains
            //put the contents of the town plan here, using Domain Specific Language
        }
    }
    ...