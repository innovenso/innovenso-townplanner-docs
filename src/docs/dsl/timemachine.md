# Time Machine

The Time Machine feature allows the town planner to render diagrams for specific points in time. It's sufficient to define
those points in time in the top level of the town plan, and to add a *lifecycle* to each element that needs to evolve over
time.

## Key Points in Time

The key points in time, for which diagrams will be rendered, are defined at the top level of the town plan.

    ...
    concepts {
        timeMachine {
            pointInTime {
                title "First Time travel experiment by Doc Brown"
                date LocalDate.of(1985, 10, 26)
            }
            pointInTime {
                title "Doc Brown first envisions the Flux Capacitor"
                date LocalDate.of(1955, 11, 5)
                render false
            }
            pointInTime {
                key "back_to_the_future_day"
                title "Marty arrives in the future"
                date LocalDate.of(2015, 10, 21)
            }
            today()
        }
    ...

A point in time has a required *title*, a required *date* of type LocalDate and an optional *render* property of type boolean.
This last one indicates whether or not diagrams should be rendered for this point in time.

As with all elements in the town plan, a unique *key* can be specified. If it is omitted, one will be generated automatically.

## Lifecycle aspects on elements

Every element in the town plan can have a *lifecycle* aspect, describing important points in time in the life of the element.

    system {
        ...
        lifecycle {
            development {
                title "Development started"
                date LocalDate.of(2003, 1, 1)
            }
            production {
                title "Deployed in production"
                date LocalDate.of(2005,1,1)
                description "On this day, we finally managed to deploy this system to production."
            }
            decommissioned {
                title "Decommissioned"
                date LocalDate.of(2027,10,31)
            }
        }
    }

A lifecycle can have the following event types:

* *due*: mostly used for projects, milestones and decisions. Indicates the due date.
* *analysis*: the element was first conceived, starting the analysis phase.
* *development*: development has started, but the element is not in production yet.
* *preProduction*: the element has been deployed in a pre-production environment.
* *production*: the element is live, in production.
* *active*: same as production, but more suited for other types of elements, that do not get deployed.
* *phaseOut*: phasing out, but still in use.
* *decommissioned*: no longer in use.
* *event*: any other meaningful date you wish to document for an element.

Some of these event types have a special meaning when rendering diagrams. Anything before *production* or *active* will
not be rendered, and the same is true for *decommissioned*, with one exception. On the first Key Point in Time after the
decommissioning, an element will still be rendered, but with a red border, to indicate the change. Elements in *phaseOut* are
rendered with a yellow border. Elements that are *active* or in *production* will be rendered with a green border on the
first Key Point in Time after they became active. Elements with no lifecycle information are considered to be immune to
time, and are always rendered.

An event in the lifecycle has a required *title*, required *date* of type LocalDate and an optional *description*. A key
is automatically generated, but can be overridden by adding the *key* property.