# Innovenso Townplanner

The Innovenso Townplanner is a set of libraries used to document a town plan.

## Enterprise Architecture As Code

The Townplanner allows you to document the entire enterprise architecture of a company, using an easy to understand
Domain Specific Language (DSL).

## I love it when a town plan comes together

This project is somewhat of an experiment gone crazy. It all started with a script that read 3 sheets from an Excel file, 
with systems, containers and relationships, and generated C4 container diagrams from that. Next came an AWS import because
I needed to give the CISO of my client at the time deployment diagrams of all our systems running in AWS.

After changing clients I piqued the interest of one of my colleagues, so I developed a UI on top of it, and after that I must have added a new
element, aspect, diagram or export every week.

The last few weekends, I've been working on the Domain Specific Language, partly because I don't really enjoy using a
web UI, but mainly because I want to benefit from all the fun things you can do with Git and IDEs. I also decided to
finally open source the tool, so more people can use it and build on it if they want.

