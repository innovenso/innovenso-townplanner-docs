# Coming soon

This project is a work in progress, and one that will keep evolving, as the needs arise, and as I find time to develop them. After all, I've got 5 kids to feed!

Some features were developed before, when it was private and a monolith, but need to be reintegrated into this iteration of the Townplanner:

* import of deployment models from AWS, and rendering of deployment diagrams
* import from Excel
* flow and sequence diagrams
* flows as part of system integrations

Some features are planned and should be coming in the next few months:

* Pace layering
* Value streams
* Target operating model (TOM)
* Team topologies
* Rendering a standalone town plan website
* Rendering a proper Technology Radar, similar to the one ThoughtWorks has.
* Adding time machine support to more diagrams
* Some more analytical tools