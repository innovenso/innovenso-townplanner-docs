# Getting Started

The simplest way to use the Townplanner is through the Gradle plugin.

## Prerequisites

You need the following software installed on your computer to use the Townplanner Gradle Plugin:

- Java Development Kit 17
- Gradle 7.3 or higher
- Graphviz, any version [supported by PlantUML](https://plantuml.com/faq)
- Blender 2.80 or higher (if you want to use Blender export)
- Git (if you want to version control your town plan)

## Project setup

### Directory structure

Create a directory for your town plan project:

    mkdir wayne-enterprises-townplan
    cd wayne-enterprises-townplan
    mkdir -p src/main/groovy/townplan
    mkdir -p src/main/resources

### Gradle configuration

Now we create a `settings.gradle` file in the root directory of the project, which will configure the name of the project. It contains only 1 line:

    rootProject.name = 'wayne-enterprises-townplan'

### Gradle build

Next we create the action build file, `build.gradle`, in the root directory of the project. We apply the Townplanner Gradle plugin, which will do all the heavy lifting for us.

> The latest version of the Townplanner Gradle plugin is ${project.version}. Replace the version in build.gradle below with the latest one if needed.

    plugins {
        id 'com.innovenso.townplanner' version '3.0.0'
    }
    
    group = 'com.wayne'
    

### The First Town Plan Zone

The town plan is divided into zones, and you need at least one of them. Actually you don't, but a townplan without
zones is empty, so not very useful. Zones must be created in a the `src/main/groovy/townplan` directory, or a subdirectory.
The reason for this is that the Townplanner scans this directory for zones to include in the town plan. See the [concepts page](concepts.md) for more information about zones.

A Zone is a *Groovy* class, extending `Zone`, and annotated with the `IncludeInTownPlan` annotation. This allows the Townplanner
to load all the concepts listed in the zone.

Let's created our first zone `src/main/groovy/townplan/WayneEnterprises.groovy`:

    package townplan
    
    import com.innovenso.townplan.dsl.Zone
    import com.innovenso.townplan.gradle.IncludeInTownplan
    
    @IncludeInTownplan
    class WayneEnterprises extends Zone {
    
        WayneEnterprises() {
            concepts {
            }
        }
    }

Now when we want to add concepts to the town plan, we put them between line 10 and 11, between the curly braces after `concepts`.

## The Town Plan

### Hello world

We're good to go! Let's add the enterprise itself, which is an element in the town plan:

    ...
    concepts {
        enterprise {
            title "Wayne Enterprises"
            description "Wayne Enterprises, Inc., also known as WayneCorp is a fictional company appearing in American comic books published by DC Comics, commonly in association with the superhero Batman. Wayne Enterprises is a large, growing multinational company."
        }
    }
    ...

As you can see, an enterprise is an *element* in the town plan, with a title and a description.

Let's render and see what we get! On the command line, type the following command: 

    gradle svg

This renders all the diagrams for the full town plan to SVG, in the `output` directory. You will
see a lot of logging output (we're working on that), and hopefully at the end Gradle will tell you
the build was successful.

The `output` directory will now contain a subdirectory `svg` as well as a zip file `wayne-enterprises-svg.zip`.
This pattern is followed by all the output types, where the directory contains the 
individual renderings and the zip file bundles them all.

When we look at the 3 SVG files that were created under `output/svg/enterprise`, it's no 
surprise that there is not much there for the moment. We'll add some more content in the next section.

![Wayne Enterprises](images/usage/step1/enterprise_wayne_enterprises.svg)

Note that we're using Archimate style diagrams, in most cases properly, but in this example a bit unorthodox,
as we are using the *physical facility* notation for the enterprise. 

### A bit more interesting town plan

When I started working on the Town Planner, I was inspired by [Structurizr and C4](https://structurizr.com/), because I really like the readability of a C4 container diagram. So let's create a model to demonstrate the C4 Container diagram,
and add a few extras on top:

First, we assign the enterprise to a variable. Remember, we are working in code, so we can do that:

    def wayneCorp = enterprise {
        title "Wayne Enterprises"
        description "Wayne Enterprises, Inc., also known as WayneCorp is a fictional company appearing in American comic books published by DC Comics, commonly in association with the superhero Batman. Wayne Enterprises is a large, growing multinational company."
    }

Now, we can reference the enterprise element further down in the town plan. Also, if we want to rename in the future, we can use the refactoring features of our IDE. 

Let's add some business capabilities:

    def superheroCapability = capability {
        title "Offering Superhero Services"
        enterprise wayneCorp

        capability {
            key "managing_secret_lairs_capability"
            title "Managing Secret Lairs"
            description "Every enterprise that serves as the cover for a superhero needs secret lairs. And when you have secret lairs, you need to manage them."
        }
    }

    capability {
        title "Enterprise Support"
        enterprise wayneCorp

        capability {
            title "Accounting"
        }
    }

As you can see, we can define the business capability map of an enterprise as a set of nested
*capability* elements. Internally, they get automatic numbering. See the [Business Capability DSL](dsl/capabilities.md) for more information.

Next, we define an Architectural Building Block that will realize the Secret Lairs capability.

    def lairManagement = buildingBlock {
        title "Lair Management"
        enterprise wayneCorp

        realizes "managing_secret_lairs_capability"
    }

We see our first relationship here: `realizes ...`. This means that the building block has a directed relationship to *managing_secret_lairs_capability*, with the default title of *realizes*.

Before we add the system that realizes this building block, let's add some technologies first. Technologies are also elements, with an architecture recommendation and a type. They can be used to generate a Technology Radar. See the [Technology DSL](dsl/technologies.md) for more information.

    def java = technology {
        title "Java"
        type TechnologyType.LANGUAGE_FRAMEWORK
        recommendation TechnologyRecommendation.ADOPT
    }

    def kubernetes = technology {
        title "Kubernetes"
        type TechnologyType.PLATFORM
        recommendation TechnologyRecommendation.ADOPT
    }

    def react = technology {
        title "React"
        type TechnologyType.LANGUAGE_FRAMEWORK
        recommendation TechnologyRecommendation.HOLD
    }

    def mongo = technology {
        title "MongoDB"
        type TechnologyType.PLATFORM
        recommendation TechnologyRecommendation.ASSESS
    }

Note that all of the above is in fact optional if you only want to render a C4 Container diagram. Let's get to the required part:

    def bcms = system {
        title "BatCave Management System"
        description "A custom Lair Management System, designed and built by Lucius Fox"

        def nemesisDb = container {
            title "Nemesis Database"
            description "A huge database with all the files of supervillains around the world"
            type ContainerType.DATABASE

            technology mongo
        }

        def nemesisMs = container {
            title "Nemesis Microservice"
            description "The single point of truth for supervillains"
            type ContainerType.MICROSERVICE

            technologies java, kubernetes

            uses nemesisDb, "fetches villain information"
        }

        def platformMs = container {
            title "Batmobile Platform Rotation Microservice"
            description "Rotates the Batmobile platform"
            type ContainerType.MICROSERVICE

            technologies java, kubernetes
        }

        def bcmsUi = container {
            key "bcms_ui"
            title "Batcave Terminal"
            description "Impressive user interface used to manage every aspect of the BatCave Management System"
            type ContainerType.TERMINAL_UI

            technology react

            uses nemesisMs, "provides UI for"
            uses platformMs, "provides rotation button for"
        }

        realizes lairManagement
    }

We have defined an IT System, called the *BatCave Management System* which has 4 containers, 1 database, 2 microservices and 1 UI. Each of them have technologies and *flow* relationships between them.

All we need now is the *business actor* to use the system:

    def batman = actor {
        title "Bruce Wayne"
        type ActorType.INDIVIDUAL
        description "I'm Batman"
        enterprise wayneCorp

        uses bcms

        uses {
            target "bcms_ui"
            title "uses"
        }
    }

Note that we explicitly indicate that Batman uses the system, as well as the container. This is a design choice based on Structurizr. We might add the feature soon that automatically adds the actor-system relationship, when an actor-container relationship exists.

So, what does all this information give us? Let's find out:

    gradle svg

#### Business Capability Map

![Wayne Enterprises Business Capability Map](images/usage/step2/enterprise_wayne_enterprises.svg)

#### Town Plan (mainly for bragging rights)

![Wayne Enterprises Town Plan](images/usage/step2/enterprise_wayne_enterprises-townplan.svg)

#### Architecture Building Block Simple View

![Lair Management](images/usage/step2/architecture_building_block_lair_management-simple.svg)

#### Architecture Building Block Detail View

![Lair Management](images/usage/step2/architecture_building_block_lair_management.svg)

#### Container Diagram - AS IS

![BatCave Management System](images/usage/step2/it_system_batcave_management_system-as-is-today.svg)

### Time Machine

You may have noticed that the Container diagram above has an interesting legend. Also, it's titled *AS IS Diagram*. Does that mean we can also generate *TO BE Diagrams*? Yes it does, let's find out how:

First, we add *key points in time* to the top level of the town plan. Each Key Point in Time will have diagrams that reflect the town plan as it was on that day, or as it will be. There is always at least 1 point in time, which is Today.

We add the key points in time to the top of the town plan:

    ...
    concepts {
        timeMachine {
            pointInTime {
                title "Before there were villains"
                date 2000, 1, 1
            }
            pointInTime {
                title "When project Batcave was deliver on time and within budget"
                date 2010, 1, 1
            }
            pointInTime {
                title "At some point in the future, the Riddler destroys the Batcave"
                date 2030, 1, 1
            }
            today()
        }

We added 2 points in time in the past, one in the future, and finally Today. Now we need to add the lifecycle to the BatCave Management System.

The database:

    def nemesisDb = container {
        ...
        lifecycle {
            development {
                title "Ra's al Ghul came to town"
                date 2003, 1, 1
            }
            production {
                title "Just in time for the Joker"
                date 2005,1,1
            }
            decommissioned {
                title "The Batcave is destroyed"
                date 2027,10,31
            }
        }
    }

The microservice:

    def nemesisMs = container {
        ...
        lifecycle {
            development {
                title "Lucius Fox gets excited over microservices"
                date 2014, 1, 1
            }
            production {
                title "Even Batman is not immune to microservices"
                date 2015,1,1
            }
            decommissioned {
                title "The Batcave is destroyed"
                date 2027,10,31
            }
        }
    }

The rotating platform was always there, because it's cool:

    def platformMs = container {
        ...
        lifecycle {
            decommissioned {
                title "The Batcave is destroyed"
                date 2027,10,31
            }
        }
    }

and the UI was the last to be added:

    def bcmsUi = container {
        ...
        lifecycle {
            development {
                title "Lucius Fox gets excited over SPA"
                date 2017, 1, 1
            }
            production {
                title "Even Batman is not immune to the SPA antipattern"
                date 2018,1,1
            }
            decommissioned {
                title "The Batcave is destroyed"
                date 2027,10,31
            }
        }
    }

Let's build the model again and generate all the diagrams:

    gradle clean svg

#### Container Diagram AS WAS at point in time 1

In the beginning, there is only a rotating platform.

![In the beginning, there was only a platform](images/usage/step3/it_system_batcave_management_system-as-was-2000-01-01.svg)

#### Container Diagram AS WAS at point in time 2

The database was added. Note the green border to indicated the addition since the last point in time.

![Then there was a database](images/usage/step3/it_system_batcave_management_system-as-was-2010-01-01.svg)

#### Container Diagram AS IS Today

In the current state, the microservice and UI were added, and have green borders again. The database did not change since the last point in time, so it has a normal border.

![Now we have a modern architecture](images/usage/step3/it_system_batcave_management_system-as-is-today.svg)

#### Container Diagram TO BE

In the end, everything is decommissioned. All decommissioned containers have a red border.

![In the end, there is nothing](images/usage/step3/it_system_batcave_management_system-to-be-2030-01-01.svg)

## Conclusion

We have only touch a part of what the Townplanner can do. As this documentation grows, check out
the pages on the DSL. I will add a lot more examples there.