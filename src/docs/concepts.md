# Core Concepts

The domain model of the Innovenso Townplanner is structured around a few concepts:

* Zones
* Elements
* Relationships
* Aspects

## Zones

A zone in the town plan is used to logically group together elements. It does not have a meaning in itself, you use it mainly to organize the town plan
in manageable parts, and in separate files if you wish. Think of it as a folder on your computer.

## Elements

Elements are basically the *boxes* in diagrams. There are many specializations of the Element, such as *IT System*, *Business Actor*, *Business Capability*, ...

Elements typically have a unique key to identify it, a title and a description. Specializations can have more properties.

## Relationships

Relationships are the *lines* in diagrams. There are quite a few types of relationships, including *flow*, *realization*, *impact*, ...

Like elements, relationships have a unique key, a title and a description.

## Aspects

Aspects provide the capability to enrich elements and relationships in the model, with extra information. There are aspects to add
*documentation*, *lifecycle*, *architecture verdict*, *SWOT analysis*, ...